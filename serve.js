// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var   cors       = require('cors');
const fileUpload = require('express-fileupload')

// Para usar con certificado ssl
var http   = require('http');
var https  = require('https');
var fs     = require('fs');

// IMPORTAR EXPRESS
const app = express();
const config = require('./config/index');
const PORT = config.PORT

// Rutas estaticas
app.use('/imagenes_surtimedix', express.static('./../../imagenes_surtimedix'));
app.use('/pdfs_surtimedix',     express.static('./../../pdf_surtimedix'));
app.use('/pdf-surtimedix',      express.static('./../../pdf'));
app.use('/recetas.pdf',         express.static('./../../recetas'));


// IMPORTAR PERMISOS
app.use(cors({ origin: '*' }));
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload()); //subir archivos


// ----IMPORTAR RUTAS---------------------------------------->
require('./routes/users.routes')(app);
require('./routes/articulos.routes')(app);
require('./routes/laboratorios.routes')(app);
require('./routes/clientes.routes')(app);
// require('./routes/correos.routes')(app);
require('./routes/entradas.routes')(app);
require('./routes/almacen.routes')(app);
require('./routes/salidas.routes')(app);
require('./routes/existencias.routes')(app);
require('./routes/documentos.routes')(app);
require('./routes/banners.routes')(app);
require('./routes/configuracion.routes')(app);
require('./routes/paypal.routes')(app);
require('./routes/direcciones.routes')(app);
require('./routes/datosfiscales.routes')(app);
require('./routes/ordenes.routes')(app);
require('./routes/pagos.routes')(app);
require('./routes/depositos.routes')(app);
require('./routes/facturas.routes')(app);

require('./routes/ciudades.routes')(app);
require('./routes/deseos.routes')(app);

require('./routes/envios.routes')(app);
require('./routes/mercadopago.routes')(app);

require('./routes/cupones.routes')(app);

require('./routes/sucursales.routes')(app);
require('./routes/caja/caja.routes')(app);
require('./routes/consultas/consultas.routes')(app);


// ----FIN-DE-LAS-RUTAS-------------------------------------->

app.listen(PORT, () => {
  console.log(`Servidor Corriendo en el Puerto ${PORT}`);
});

