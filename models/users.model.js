const sql = require("./db.js");

// constructor
const usuarios = function(user) {
  this.email     = user.email;
  this.nomuser   = user.nomuser;
  this.estatus   = user.estatus;
  this.nomper    = user.nomper;
  this.password  = user.password;
  this.nivel     = user.nivel;
  this.estatus   = user.estatus;
};
// VARIABLES PARA QUERYS

usuarios.existeUsuario = ( usuario ) => {
  return new Promise(( resolve, reject )=>{
    sql.query(`SELECT * FROM usuariosweb WHERE email = ? AND estatus = 2
      OR nomuser = ? AND estatus = 2;`, [ usuario, usuario ], (err, res) => {
      if (err) {
        return reject( err.sqlMessage );
      }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};

usuarios.validarPassword = ( usuario, password ) => {
  return new Promise(( resolve, reject )=>{
    sql.query(`SELECT u.*, s.sucursal FROM usuariosweb u
      LEFT JOIN sucursales s ON s.idsucursales = u.idsucursal
      WHERE u.email = ? AND u.password = ? OR u.nomuser = ? AND u.password = ?;`, 
      [ usuario, password, usuario, password ], (err, res) => {
      if (err) {
        console.log( err )
        return reject( err.sqlMessage );
      }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};

// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
  usuarios.updatexId = (id, user, result)=>{
    sql.query("UPDATE usuariosweb SET nomuser = ?, email = ?, password = ?,nivel = ?, estatus=?, nomper=? WHERE idusuariosweb = ?", 
              [user.nomuser, user.email, user.password, user.nivel, user.estatus, user.nomper, id], (err, res) => 
      {
        if (err) { 
          console.log("error: ", err);
          result(null, err);
          return;
        }
        if (res.affectedRows == 0) {
          // not found user with the id
          result({ kind: "not_found" }, null);
          return;
        }
        console.log("Se actualizo el user: ", { id: id, ...user });
        result(null, { id: id, ...user });
      }
    );
  };

  usuarios.deleteSchoolsxUser = (id, result) => {
    sql.query("DELETE FROM usuariosweb WHERE id_user = ?", id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log(`deleted ${res.affectedRows} user`);
      result(null, res);
    });
  };

  usuarios.createSchoolsxUser = (id,school, result) => {
    console.log('crear escuela x usuario');
    sql.query("INSERT INTO user_schools(id_user, id_school)VALUE(?,?)",[id,school], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      // console.log("created user: ", { id: res.insertId, ...newUser });
      // result(null, res);
    });
  };
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------

  usuarios.OlvideContra = (data,result) =>{
    sql.query(`SELECT idusuariosweb, email,nomuser FROM usuariosweb WHERE email = ?`,[data.email], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log("email: ", res);
      result(null, res);
    });
  };

  usuarios.passwordExtra = (user, result)=>{
    sql.query(` UPDATE usuariosweb SET passextra = ? WHERE idusuariosweb = ?`, [ user.codigo, user.id],(err, res) =>  {
        if (err) { result(null, err); return; }
        if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
        result(null, "Usuario actualizado correctamente");
      }
    );
  };
// --- SIN USAR---->
usuarios.create = (newUser, result) => {
  console.log('crear un usuario', newUser);
  sql.query("INSERT INTO usuariosweb SET ?", newUser, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created user: ", { id: res.insertId, ...newUser });
    result(null, { id: res.insertId, ...newUser });
  });
};

usuarios.findById = (userid, result) => {
  console.log("Recibo", userid)
  sql.query(`SELECT * FROM usuariosweb WHERE idusuariosweb = ${userid}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

usuarios.getxEmail = (data, result) => {
  if(data.nomuser == ''){
    sql.query(`SELECT * FROM usuariosweb WHERE email = ?`,[data.email], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log("found user: ", res[0]);
        result(null, res[0]);
        return;
      }
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }else{
    sql.query(`SELECT * FROM usuariosweb WHERE email = ? OR nomuser = ?`,[data.email, data.nomuser], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log("found user: ", res[0]);
        result(null, res[0]);
        return;
      }
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }
  
};

usuarios.getAll = result => {
  sql.query("SELECT * FROM usuariosweb", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("usuarios: ", res);
    result(null, res);
  });
};

usuarios.updateById = (id, user, result) => {
  sql.query("UPDATE usuariosweb SET email = ?, name = ?, active = ? WHERE id = ?", [user.email, user.name, user.active, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated user: ", { id: id, ...user });
      result(null, { id: id, ...user });
    }
  );
};

usuarios.remove = (id, result) => {
  sql.query("DELETE FROM usuariosweb WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

usuarios.removeAll = result => {
  sql.query("DELETE FROM usuariosweb", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} user`);
    result(null, res);
  });
};

usuarios.activarUsuario = (idusuario, result)=>{
  sql.query( `UPDATE usuariosweb SET estatus = 2 WHERE idusuariosweb = ?`, [idusuario], (err, res) => {
      if (err) { console.log("error: ", err);  result(null, err); return;  } // SI OCURRE UN ERROR LO RETORNO
      if (res.affectedRows == 0) { result({ kind: "No encontrado" }, null); return; } // SI NO ENCUENTRO EL ID

      console.log("Se actualizo el usuario: ", { idusuario: idusuario});
      result(null, { idusuario: idusuario});
    }
  );
};

module.exports = usuarios;