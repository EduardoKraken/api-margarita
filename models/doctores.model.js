const sql = require("./db.js");

// constructor
const doctores = function(lab) {
    this.idlaboratorios   = lab.idlaboratorios;
    this.nomlab           = lab.nomlab;
    this.estatus          = lab.estatus;
};

// Agregar un laboratorio
doctores.addDoctores = (n, result) => {
  sql.query(`INSERT INTO doctores(nombre,cedula,telefono,calle,numeroext,numint,colonia,cp,estado,estatus)VALUES(?,?,?,?,?,?,?,?,?,?)`,
             [n.nombre,n.cedula,n.telefono,n.calle,n.numeroext,n.numint,n.colonia,n.cp,n.estado,n.estatus], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};


// Actualizar laboratorios
doctores.updateDoctores = (id, n, result) => {
  sql.query(` UPDATE doctores SET nombre=?,cedula=?,telefono=?,calle=?,numeroext=?,numint=?,colonia=?,cp=?,estado=?,estatus=?
                WHERE iddoctores = ?`,[n.nombre,n.cedula,n.telefono,n.calle,n.numeroext,n.numint,n.colonia,n.cp,n.estado,n.estatus, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...n });
    }
  );
};

doctores.getDoctores = (result)=>{
  sql.query("SELECT * FROM doctores",(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

doctores.getDoctoresActivo = (result)=>{
  sql.query("SELECT *, CONCAT( nombre, ' ', cedula ) AS todo FROM doctores WHERE estatus = 1;",(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};


module.exports = doctores;