const sql = require("./db.js");

// constructor

const Salidas = entradas => {
  this.idsalidas    = entradas.idsalidas;
  this.codigo       = entradas.codigo;
  this.caducidad    = entradas.caducidad;
  this.cant         = entradas.cant;
  this.lote         = entradas.lote;
  this.fecha        = entradas.fecha;
  this.recibido     = entradas.recibido;
  this.entrego      = entradas.entrego;
  this.nomart       = entradas.nomart;
  this.nomuser      = entradas.nomuser;
};


Salidas.addSalidas = (art, result) => {
  sql.query(`INSERT INTO salidas (codigo,caducidad,cant,lote,fecha,recibio,entrego)
                                  VALUES(?,?,?,?, ?,?,?)`,
    [art.codigo, art.caducidad,art.cant,art.lote,art.fecha,art.recibio,art.entrego], (err, res) => {  
    if (err) {
      result(err, null);
      return;
    }
    result(null, { id: res.insertId, ...art });
  });
};

Salidas.getSalidasAll = result => {
  sql.query(`SELECT * FROM salidas;`, (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Salidas.getSalidasFecha = (fecha, result) => {
  sql.query(`SELECT COUNT(dv.cantidad) AS cantidad, dv.producto, dv.idproductos, a.codigo FROM detalle_venta dv
      LEFT JOIN ventas v ON v.idventas = dv.idventas
      LEFT JOIN arts a ON a.id = dv.idproductos
      WHERE v.estatus = 1 AND v.deleted = 0 AND DATE(dv.fecha_creacion) BETWEEN ? AND ?
      GROUP BY dv.idproductos, dv.producto, dv.idproductos, a.codigo;`,
    [fecha.inicio,fecha.fin], (err, res) => {  
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Salidas.addSalidasExternas = (art, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO salidas_externas (idproductos,cantidad,idusuario,motivo)VALUES(?,?,?,?)`,
      [art.idproductos, art.cantidad,art.idusuario,art.motivo], (err, res) => {  
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve({ id: res.insertId, ...art });
    });
  })
};

Salidas.updateAlmacen = (idproductos, cant ) => {
  return new Promise((resolve, reject) => {
    sql.query(`UPDATE almacen SET cant = cant - ? WHERE idalmacen > 0 AND idarts = ?;` , [cant, idproductos],(err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve({ idproductos, cant });
    });
  })
};

Salidas.getSalidasExternas = (fecha, result) => {
  sql.query(`SELECT s.*, a.nomart, u.nomper FROM salidas_externas s
      LEFT JOIN arts a ON a.id = s.idproductos
      LEFT JOIN usuariosweb u ON u.idusuariosweb = s.idusuario;`,
    [fecha.inicio,fecha.fin], (err, res) => {  
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Salidas.getSalidasExternasFechas = (inicio, fin) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT s.idproductos, s.idproductos, a.nomart, u.nomper, SUM(s.cantidad) AS cantidad FROM salidas_externas s
      LEFT JOIN arts a ON a.id = s.idproductos
      LEFT JOIN usuariosweb u ON u.idusuariosweb = s.idusuario
      WHERE DATE(s.fecha_creacion) BETWEEN ? AND ?
      GROUP BY s.idproductos;`,
      [inicio, fin], (err, res) => {  
      if (err) {
        return reject({ message: err.sqlMessage })
      }
      resolve(res);
    });
  });
};

module.exports = Salidas;