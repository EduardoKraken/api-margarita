const sql = require("./db.js");

// constructor

const Ordenes = orden => {
  this.iddocum        = orden.iddocum;
  this.idusuariosweb  = orden.idusuariosweb;
  this.direccion      = orden.direccion;
  this.importe        = orden.importe;
  this.descuento      = orden.descuento;
  this.subtotal       = orden.subtotal;
  this.total          = orden.total;
  this.iva            = orden.iva;
  this.folio          = orden.folio;
  this.estatus        = orden.estatus;
  this.nota           = orden.nota;
  this.divisa         = orden.divisa;
  this.hora           = orden.hora;
  this.fecha          = orden.fecha;
  this.fechapago      = orden.fechapago;
  this.refer          = orden.refer;
  this.tipodoc        = orden.tipodoc;
  this.Movim         = orden.Movim;
};


Ordenes.addOrden = (o, result) => {
  sql.query(`INSERT INTO docum SET idusuariosweb= ? ,direccion= ? ,importe= ? ,descuento= ? ,subtotal= ? ,total= ? ,iva= ? ,folio= ? ,estatus= ? ,nota= ? ,divisa= ? ,hora= ? ,fecha= ? ,fechapago= ? ,refer= ? ,tipodoc = ? `,
  	[o.idusuariosweb ,o.direccion ,o.importe ,o.descuento ,o.subtotal ,o.total ,o.iva ,o.folio ,o.estatus ,o.nota ,o.divisa ,o.hora ,o.fecha ,o.fechapago ,o.refer ,o.tipodoc], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }else{
    	var id = res.insertId
	    var folio = 'SURTI-' + id
	    sql.query(`UPDATE docum SET folio = ? WHERE iddocum = ?`,[folio, res.insertId], (err, res) => {  
		    if (err) {
		      console.log("error: ", err);
		      result(err, null);
		      return;
		    }else{
			    o.movim.forEach((element,index)=>{
			    	sql.query(`INSERT INTO movim SET codigo= ? ,foto= ? ,descrip= ? ,cantidad= ? ,precio= ? ,descuento= ? ,iva= ? ,neto = ?, iddocum= ?`,
					  	[element.codigo, element.foto ,element.descrip ,element.cantidad ,element.precio1 ,element.descuento ,element.iva ,element.total,id], (err, res) => {  
					    if (err) {
					      console.log("error: ", err);
					      result(err, null);
					      return;
					    }
					  });
					  if(index == (o.movim.length-1)){
					  	console.log("Crear Grupo: ", { id: res.insertId, ...o });
			    		result(null, { id: res.insertId, ...o });
					  }
			    })
		    }
		  });

    }


  });
};

// Falta actualizar el folio y agregar el Movim
Ordenes.getDocumen = () => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM docum`,(err, res) => { 
      if (err) {
        reject(err);
        return;
      }
      resolve(res)
    });
  })
};

// Falta actualizar el folio y agregar el Movim
Ordenes.getDocumenCli = ( id ) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM docum WHERE idcliente = ${id}`,(err, res) => { 
      if (err) {
        reject(err);
        return;
      }
      resolve(res)
    });
  })
};


Ordenes.getMovimientos = () => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT m.*, a.nomart, a.codigo, a.descrip, a.largo, a.ancho, a.alto, a.peso, a.envoltorio FROM movim m
      LEFT JOIN arts a ON a.id = m.id;`,(err, res) => { 
      if (err) {
        reject(err);
        return;
      }
      resolve(res)
    });
  })
};

Ordenes.getDirecciones = () => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM pago_direcciones;`,(err, res) => { 
      if (err) {
        reject(err);
        return;
      }
      resolve(res)
    });
  })
};

Ordenes.getDireccionesFac = () => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM pago_facturacion;`,(err, res) => { 
      if (err) {
        reject(err);
        return;
      }
      resolve(res)
    });
  })
};

Ordenes.getClientesOrden = () => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM clientes;`,(err, res) => { 
      if (err) {
        reject(err);
        return;
      }
      resolve(res)
    });
  })
};

Ordenes.getPagosMercaddoPago = () => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM pagos_mercadopago;`,(err, res) => { 
      if (err) {
        reject(err);
        return;
      }
      resolve(res)
    });
  })
};

Ordenes.getOrdenes = (id,result) => {
  sql.query(`SELECT * FROM docum WHERE idcliente = ?;`,[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};


Ordenes.getOrdenesCliente = (result) => {
  sql.query(`SELECT * FROM docum ;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

Ordenes.getOrdenId = (id, result) => {
  sql.query(`SELECT * FROM docum WHERE iddocum = ?;`,[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

Ordenes.getMovim = (id, result) => {
  sql.query(`SELECT * FROM movim WHERE iddocum = ?;`,[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

// Ordenes.getEntradasFecha = (fecha, result) => {
//   sql.query(`SELECT e.identradas, a.nomart, e.codigo, e.fechaentr, e.lote, e.cant, e.caducidad, e.proveedor, e.factura 
//         FROM  entradas e INNER JOIN arts a ON e.codigo = a.codigo WHERE fechaentr BETWEEN ? AND ?`,
//     [fecha.inicio,fecha.fin], (err, res) => {  
//     	console.log(sql)
//     if (err) {
//       console.log("error: ", err);
//       result(null, err);
//       return;
//     }
//     console.log("entradas: ", res);
//     result(null, res);
//   });
// };

Ordenes.updateOrden = (id, ent, result) => {
  sql.query(` UPDATE docum SET estatus = ?, nota = ? WHERE iddocum = ?`, [ent.estatus, ent.nota, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated enticulos: ", { id: id, ...ent });
      result(null, { id: id, ...ent });
    }
  );
};

Ordenes.updateOrdenEnvio = (id, ent, result) => {
  sql.query(` UPDATE docum SET trackingnumber = ? WHERE iddocum = ?`, [ent.trackingnumber,id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated enticulos: ", { id: id, ...ent });
      result(null, { id: id, ...ent });
    }
  );
};


Ordenes.getRastreoCliente = () => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM rastreo;`,(err, res) => { 
      if (err) {
        reject(err);
        return;
      }
      resolve(res)
    });
  })
};
module.exports = Ordenes;