const sql = require("../db.js");

//const constructor
const consultas = function(operadores) {};

consultas.getConsultasFecha = ( fecha_inicio, fecha_fin ) => {
	return new Promise((resolve,reject)=>{
	  sql.query(`SELECT c.*, s.servicio FROM consultas c
      LEFT JOIN servicios s ON s.idservicios = c.idservicios
      WHERE DATE(c.fecha_creacion) BETWEEN ? AND ? AND c.deleted = 0
	  	ORDER BY c.estatus ASC;`,[ fecha_inicio, fecha_fin ], (err, res) => {
	    if( err ){ return reject({message: err.sqlMessage })}
      resolve( res)
	  });
	});
};

consultas.addConsulta = ( c ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO consultas ( nombre_paciente, idservicios, costo, id_caja )VALUES( ?, ?, ?, ? );`,
    	[ c.nombre_paciente, c.idservicios, c.costo, c.id_caja ],(err, res) => {

      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ id: res.insertId, ...c })

    })
  })
}

consultas.updateConsultas = ( estatus, deleted, id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE consultas SET estatus = ?, deleted = ?  WHERE idconsultas = ?;`,
      [ estatus, deleted, id ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ estatus, deleted, id })
    })
  })
}

consultas.updateDevolucion = ( devolucion, id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE consultas SET devolucion = ? WHERE idconsultas = ?;`,
      [ devolucion, id ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ devolucion, id })
    })
  })
}

consultas.getPacientes = ( ) => {
	return new Promise((resolve,reject)=>{
	  sql.query(`SELECT *, CONCAT(nombre, ' ',apellido_paterno, ' ', IFNULL(apellido_materno,'')) AS nombre_completo FROM pacientes
	  	ORDER BY nombre_completo;`, (err, res) => {
	    if( err ){ return reject({message: err.sqlMessage })}
      resolve( res)
	  });
	});
};

consultas.addPaciente = ( c ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO pacientes ( nombre, apellido_paterno, apellido_materno, edad, telefono, sexo, fecha_nacimiento )VALUES( ?, ?, ?, ?, ?, ?, ? );`,
    	[ c.nombre, c.apellido_paterno, c.apellido_materno, c.edad, c.telefono, c.sexo, c.fecha_nacimiento ],(err, res) => {

      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ id: res.insertId, ...c })

    })
  })
}

consultas.updatePaciente = ( estatus, deleted, id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE consultas SET estatus = ?, deleted = ?  WHERE idconsultas = ?;`,
      [ estatus, deleted, id ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ estatus, deleted, id })
    })
  })
}

consultas.addReceta = ( c ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO recetas ( idpacientes, peso, notas, diagnostico )VALUES( ?, ?, ?, ? );`,
    	[ c.idpacientes, c.peso, c.notas, c.diagnostico ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ id: res.insertId, ...c })

    })
  })
}

consultas.getHora = ( ) => {
	return new Promise((resolve,reject)=>{
	  sql.query(`SELECT NOW() AS fecha;`, (err, res) => {
	    if( err ){ return reject({message: err.sqlMessage })}
      resolve( res[0] )
	  });
	});
};

consultas.getServicios = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM servicios WHERE deleted = 0;`, (err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve( res )
    });
  });
};

consultas.addServicios = ( c ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO servicios ( servicio, costo  )VALUES( ?, ? );`,
      [ c.servicio, c.costo ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ id: res.insertId, ...c })

    })
  })
}

consultas.updateServicios = ( c, id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE servicios SET servicio = ?, costo = ?  WHERE idservicios = ?;`,
      [ c.servicio, c.costo, id ],(err, res) => {
      if( err ){ return reject({message: err.sqlMessage })}
      resolve({ ...c, id })
    })
  })
}

module.exports = consultas;