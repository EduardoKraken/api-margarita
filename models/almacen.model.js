const sql = require("./db.js");

// constructor

const Alamacen = almacen => {
  this.codigo      = almacen.codigo;
  this.lote        = almacen.lote;
  this.cant        = almacen.cant;
  this.caducidad   = almacen.caducidad;
};

Alamacen.addAlmacen = (ent, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO almacen (idarts,cant)
                                  VALUES(?,?)`,
      [ent.idarts,ent.cant], (err, res) => { 
      if (err) {
        reject(err)
        return;
      }
      resolve(res)
    });
  })
};

Alamacen.getAlmacen = (art, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM almacen WHERE idarts = ?`, [art.idarts],(err, res) => {
      if (err) {
        reject(err)
        return;
      }
      resolve(res)
    });
  })
};

Alamacen.updateAlmacen2 = (art, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`UPDATE almacen SET cant = cant + ? WHERE idalmacen > 0 AND idarts = ?` , 
      [art.cant, art.idarts],(err, res) => {
      if (err) {
        reject(err)
        return;
      }
      resolve(res)
    });
  })
};


Alamacen.getAlmacenList = result => {
  sql.query(`SELECT * FROM almacen a 
    LEFT JOIN arts ar ON ar.id = a.idarts
    WHERE a.cant > 0;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("almacen: ", res);
    result(null, res);
  });
};

Alamacen.updateAlmacen = (id, alm, result) => {
  sql.query(`UPDATE almacen SET cant = ? WHERE idalmacen = ? `, [alm.cant, id],(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("updated almacen: ", { id: id, ...alm });
    result(null, { id: id, ...alm });
  });
};

Alamacen.validarAlmacen = (ent, result) => {
  sql.query(`SELECT a.codigo, a.id, SUM(alm.cant) AS cant , a.nomart, 
            (SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1) AS foto 
    FROM almacen alm LEFT JOIN arts a ON a.codigo = alm.codigo WHERE a.id IN (?) GROUP BY a.codigo, a.id;`,
    [ent], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("almacen: ", res);
    result(null, res);
  });
};

module.exports = Alamacen;