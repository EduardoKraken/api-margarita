const sql = require("./db.js");

// constructor
const Clientes = function(clientes) {
  this.idcliente = clientes.idcliente;
  this.nombre    = clientes.nombre;
  this.password  = clientes.password;
  this.telefono  = clientes.telefono;
  this.estatus   = clientes.estatus;
};

Clientes.login_cliente = (loger,result) =>{
  sql.query("SELECT * FROM clientes WHERE email = ? AND password = ? or email = ? AND passextra = ?"
    , [loger.email, loger.password, loger.email, loger.password],(err, res) => {
    if (err) { 
      console.log("error: ", err); 
      result(err, null);
      return 
    }
    console.log('resultado sesion', res[0])
    result(null, res[0])
  });
};
Clientes.addCliente = (c, result) => {
	sql.query(`INSERT INTO clientes(nombre,email,password,telefono)VALUES(?,?,?,?)`,[c.nombre,c.email,c.password,c.telefono], 
    (err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    console.log("Crear Grupo: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
	});
};
Clientes.getClienteId = (params, result)=>{
	sql.query(`SELECT SELECT idcliente, nombre, email, telefono,password, estatus FROM clientes 
    FROM clientes WHERE idcliente=?`, [params.idweb], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};
Clientes.getClientes = result => {
  sql.query(`SELECT idcliente, nombre, email, telefono,password, estatus FROM clientes`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("clientes: ", res);
    result(null, res);
  });
};
Clientes.updateCliente = (cli, result) => {
  sql.query(` UPDATE clientes SET nombre=?, apellido=?, usuario =?, email=?,password=?,telefono=?
                WHERE idcliente = ?`, 
              [cli.data.nombre, cli.data.apellido, cli.data.usuario,cli.data.email,cli.password,cli.data.telefono, cli.data.idcliente],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      // console.log("updated cli: ", { id: id, ...cli.data });
      result(null, true);
    }
  );
};

Clientes.OlvideContraCliente = (data,result) =>{
  sql.query(`SELECT idcliente,nombre,email FROM clientes WHERE email = ?`,[data.email], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("email cliente: ", res);
    result(null, res);
  });
};
Clientes.passwordExtraCliente = (user, result)=>{
  sql.query(` UPDATE clientes SET passextra = ? WHERE idcliente = ?`, [ user.codigo, user.id],(err, res) =>  {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
      result(null, "Cliente actualizado correctamente");
    }
  );
};

module.exports = Clientes;