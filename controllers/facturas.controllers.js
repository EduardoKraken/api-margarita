
const Facturas = require("../models/facturas.model.js");

// Crear un cliente
exports.addFacturas = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Facturas.addFacturas(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

exports.getFacturasId = (req, res)=>{
    Facturas.getFacturasId(req.params.idfacturas,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};

exports.getFacturas = (req,res) => {
    Facturas.getFacturas(req.params.idpagos,(err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};

exports.updateFacturas = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Facturas.updateFacturas( req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el cliente con el id `
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el cliente con el id "
				});
			}
		} 
		else res.send(data);
	}
	);
}


// Delete a users with the specified usersId in the request
exports.deleteFacturas = (req, res) => {
  Direcciones.deleteFacturas(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};







