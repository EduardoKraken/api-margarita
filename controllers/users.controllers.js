const usuarios = require("../models/users.model.js");

// Validar el acceso del usuario
exports.session = async (req, res) => {
  try{
    // Desestrucutramos los datos
    const { usuario, password } = req.body

    // Consultamos los datos del usuario
    const existeUsuario = await usuarios.existeUsuario( usuario ).then( response => response )

    // Si el usuario no existe, hay que retornar que no existe
    if(!existeUsuario){ return res.status( 400 ).send( { message: 'Usuario no existe' } ) }

    // Validar las contraseñas
    const validarPassword = await usuarios.validarPassword( usuario, password ).then( response => response )

    // Si el usuario no existe, hay que retornar que no existe
    if(!validarPassword){ return res.status( 400 ).send( { message: 'Contraseña incorrecta' } ) }

    res.send(validarPassword)

  }catch( error ){
    res.status( 500 ).send( { message: 'Error al intentar validar el usuario', error } )
  }
};

// PROCESO PARA ACTUALIZAR PERFIL
exports.updatexId = (req, res) =>{
  usuarios.updatexId(req.params.idusuariosweb, req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el usuario con el id ${req.params.idusuariosweb }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el usuario con el id" + req.params.idusuariosweb 
        });
      }
    }else res.send(data);
  })
};

// Crear y salvar un nuevo usuario
exports.create = (req, res) => {
	
  if(!req.body){ // Validacion de request
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }
  
  // const user = new User({ // Crear un usuario
  // 	email     : req.body.email,
  //   nomuser   : req.body.nomuser,
  // 	nomper    : req.body.nomper,
  //   nivel     : req.body.nivel,
  // 	password  : req.body.password,
  //   estatus   : req.body.estatus
  // })
  
  usuarios.create(req.body, (err, data)=>{ // Guardar el CLiente en la BD
  	
  	if(err) // EVALUO QUE NO EXISTA UN ERROR
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Usuario"
  		})
  	else res.send(data)

  })
};

exports.findAll = (req, res) => { // Retrieve all userss from the database.
  usuarios.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};

// Find a single users with a usersId
exports.findOne = (req, res) => {
  usuariosfindById(req.params.userId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el cliente con el id ${ req.params.userId }.`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el usuario con el id" + req.params.userId 
        });
      }
    } else res.send(data);
  });
};

// Find a single users with a usersId
exports.getxEmail = (req, res) => {
  usuariosgetxEmail(req.body,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(202).send({
          message: `No encontre el usuario`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el usuario" 
        });
      }
    } else res.send(data);
  });
};

// Update a users identified by the usersId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }

  usuariosupdateById(req.params.userId, new User(req.body),(err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(400).send({
            message: `No encontre el usuario con el id ${req.params.userId }.`
          });
        } else {
          res.status(500).send({
            message: "Error al actualizar el usuario con el id" + req.params.userId 
          });
        }
      } 
      else res.send(data);
    }
  );
};

// Delete a users with the specified usersId in the request
exports.delete = (req, res) => {
  usuariosremove(req.params.usersId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.usersId}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.usersId
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};

// Delete all userss from the database.
exports.deleteAll = (req, res) => {
  usuariosremoveAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al eliminar a todos los clientes."
      });
    else res.send({ message: `Todos los usuarios se eliminarion correctamente!` });
  });
};


exports.activarUsuario = (req, res) => {
  usuariosactivarUsuario(req.params.idusuario,(err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `No encontre el usuario con el id ${req.params.idusuario }.`
          });
        } else {
          res.status(500).send({
            message: "Error al actualizar el usuario con el id" + req.params.idusuario 
          });
        }
      } 
      else res.send(data);
  });
};

exports.OlvideContra = (req, res) => {
  usuariosOlvideContra(req.body,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(202).send({
          message: `No encontre el correo`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el correo" 
        });
      }
    } else res.send(data);
  });
};

exports.passwordExtra = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  usuariospasswordExtra(req.body,(err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(400).send({
            message: `No encontre el usuario con el id ${req.body.id }.`
          });
        } else {
          res.status(500).send({
            message: "Error al actualizar el usuario con el id" + req.body.id 
          });
        }
      } 
      else res.send(data);
    }
  );
};

