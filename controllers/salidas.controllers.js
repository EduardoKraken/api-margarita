const Salidas = require("../models/salidas.model.js");
const Articulos = require("../models/articulos.model.js");


// traer las entradas por fecha
exports.getSalidasFecha = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Salidas.getSalidasFecha(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

// agregar una entrada
exports.addSalidas = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Salidas.addSalidas(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

exports.getSalidasAll =  (req,res)=>{
  Salidas.getSalidasAll((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    }else{
    	res.send(data)
    } 
  });
};

exports.getSalidasExternas = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  Salidas.getSalidasExternas(req.body, (err, data)=>{
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el cliente"
      })
    else res.send(data)
  })
};

// agregar una entrada
exports.addSalidasExternas = async (req, res) => {

  try{

    if(!req.body){
      res.status(400).send({
        message:"El Contenido no puede estar vacio"
      });
    }

    const addSalida     = await Salidas.addSalidasExternas( req.body ).then( response => response )

    const { idproductos, cantidad } = req.body

    const updateAlmacen = await Salidas.updateAlmacen( idproductos, cantidad ).then( response => response )

    res.send({message: 'Salida agregada correctamente' })

  }catch( error ){
    res.status( 500 ).send({message: error ? error.message : 'Error en el servidor' })
  }
};


exports.getSalidasExternasFechas = async (req, res) => {

  try{

    const { inicio, fin } = req.body
    const habilitarGroup = await Articulos.habilitarGroup( ).then( response => response )
    const addSalida     = await Salidas.getSalidasExternasFechas( inicio, fin ).then( response => response )

    res.send(addSalida)

  }catch( error ){
    res.status( 500 ).send({message: error ? error.message : 'Error en el servidor' })
  }
};
