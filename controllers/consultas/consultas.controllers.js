const consultas = require("../../models/consultas/consultas.model.js");
const newPDF    = require('../../helpers/generarPDF');


// llamara todos los Categorias excepto los deleted = 1
exports.getConsultasFecha = async (req, res) => {
  try{

  	const { fecha_inicio, fecha_fin } = req.body
  	
  	const listConsultas = await consultas.getConsultasFecha( fecha_inicio, fecha_fin ).then( response => response )

  	res.send( listConsultas )

  }catch( error ){
  	res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
  
};

exports.addConsulta = async(req, res) => {
  try {

    const data = await consultas.addConsulta( req.body ).then( response => response )

		res.send(data);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateConsultas = async(req, res) => {
  try {

  	const { id } = req.params
  	const { estatus, deleted } = req.body 

    const data = await consultas.updateConsultas( estatus, deleted, id ).then( response => response )

		res.send(data);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateDevolucion = async(req, res) => {
  try {

    const { id } = req.params
    const { devolucion } = req.body 

    const data = await consultas.updateDevolucion( devolucion, id ).then( response => response )

    res.send(data);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getPacientes = async (req, res) => {
  try{

  	const listConsultas = await consultas.getPacientes( ).then( response => response )

  	res.send( listConsultas )

  }catch( error ){
  	res.status(500).send({message: error ? error.message : 'Error en el servidor'})
  }
  
};

exports.addPaciente = async(req, res) => {
  try {

    const data = await consultas.addPaciente( req.body ).then( response => response )

		res.send(data);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updatePaciente = async(req, res) => {
  try {

  	const { id } = req.params
  	const { estatus, deleted } = req.body 

    const data = await consultas.updatePaciente( estatus, deleted, id ).then( response => response )

		res.send(data);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};


exports.addReceta = async(req, res) => {
  try {

    const data = await consultas.addReceta( req.body ).then( response => response )

    const hora = await consultas.getHora().then( response => response )

    let payload = {
    	...req.body,
    	id: data.id,
    	hora: hora.fecha
    }

		await newPDF( payload );

		res.send( payload );

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getServicios = async(req, res) => {
  try {

    const data = await consultas.getServicios( ).then( response => response )

    res.send( data );

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};


exports.addServicios = async(req, res) => {
  try {

    const data = await consultas.addServicios( req.body ).then( response => response )

    res.send( data );

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};


exports.updateServicios = async(req, res) => {
  try {

    const data = await consultas.updateServicios( req.body, req.params.id ).then( response => response )

    res.send( data );

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};
