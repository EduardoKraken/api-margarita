
const doctores = require("../models/doctores.model.js");


exports.addDoctores = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  doctores.addDoctores(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Laboratorio"
  		})
  	else res.send(data)

  })
};

exports.getDoctores = (req,res) => {
    doctores.getDoctores((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

exports.updateDoctores = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	doctores.updateDoctores(req.params.id, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el laboratorio con el id ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el laboratorio con el id" + req.params.id 
				});
			}
		} 
		else res.send(data);
	}
	);
}


exports.getDoctoresActivo = (req,res) => {
    doctores.getDoctoresActivo((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};


