const Sucursales = require("../models/sucursales.model.js");

// agregar una entrada
exports.addSucursales = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Sucursales.addSucursales(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la Sucursal"
  		})
  	else res.send(data)
  })
};


// traer las entradas por fecha
exports.getSucursales = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Sucursales.getSucursales((err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la Sucursal"
  		})
  	else res.send(data)
  })
};


exports.updateSucursales = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  
 Sucursales.updateSucursales(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre la sucursal con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar la sucursal con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 });
};

exports.getSucursalesActivas = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  Sucursales.getSucursalesActivas((err, data)=>{
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear la Sucursal"
      })
    else res.send(data)
  })
};





