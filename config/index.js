const dotenv = require('dotenv').config();

console.log('/***************** SUC. #1 ******************/')
module.exports = {
	ENV: process.env.ENV,
	PORT: process.env.PORT || 3008,
}
