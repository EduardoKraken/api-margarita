module.exports = app => {
  const sucursales = require('../controllers/sucursales.controllers') // --> ADDED THIS
  // movil
  app.get("/sucursales.all",           sucursales.getSucursales);  
  app.post("/sucursales.add",          sucursales.addSucursales);  
  app.put("/sucursales.update/:id",    sucursales.updateSucursales);
  app.get('/sucursales.activas',       sucursales.getSucursalesActivas)

};