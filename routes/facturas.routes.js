module.exports = app => {
  const facturas = require('../controllers/facturas.controllers') // --> ADDED THIS

  app.post("/facturas.add",                   facturas.addFacturas);   // CREAR UN NUEVO GRUPO
  app.get("/facturas/:idfacturas",            facturas.getFacturasId);   
  app.get("/facturas.list/:idpagos",          facturas.getFacturas);   
  app.put("/facturas.update/",                 facturas.updateFacturas);
  app.delete("/facturas.delete/:id",          facturas.deleteFacturas);

};