module.exports = app => {
  const consultas = require('../../controllers/consultas/consultas.controllers') 

  app.post("/consultas.fecha"         , consultas.getConsultasFecha);
  app.post("/consultas.add"           , consultas.addConsulta);
  app.put("/consultas.update/:id"     , consultas.updateConsultas);
  app.put("/consultas.devolucion/:id" , consultas.updateDevolucion);

  app.get("/pacientes.list"           , consultas.getPacientes);
  app.post("/pacientes.add"           , consultas.addPaciente);
  app.put("/pacientes.update/:id"     , consultas.updatePaciente);
  

  app.post("/receta.add"              , consultas.addReceta);

  app.get("/consultas.servicios"            , consultas.getServicios)
  app.post("/consultas.servicios.add"       , consultas.addServicios)
  app.put("/consultas.servicios.update/:id" , consultas.updateServicios)

};