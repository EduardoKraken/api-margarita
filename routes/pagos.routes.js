module.exports = app => {
  const pagos = require('../controllers/pagos.controllers') // --> ADDED THIS

  app.post("/pagos.add",          pagos.addPagos);   // CREAR UN NUEVO GRUPO
  app.get("/pagos/:idpagos",      pagos.getPagosId);   
  app.get("/pagos.list",          pagos.getPagos);   
  app.put("/pagos.update",        pagos.updatePagos);

};