const PDFDocument = require('pdfkit');

const fs = require('fs');

const newPDF = ({
  id,
  nombre_completo,
  sexo,
  edad,
  peso,
  telefono,
  notas,
  fecha,
  diagnostico,
  hora,
  idusuario
}) => {

  // Crear un nuevo documento PDF
  const doc = new PDFDocument();

  // Nombre del y lugar del documento con número de folio
  doc.pipe(fs.createWriteStream(`../../recetas/${ id }.pdf`));

  doc.image('../../recetas/logo.png', 260, 30, { width: 80 });

  doc.moveDown();

  doc.fontSize(16).text( `Farmacia Surtimedix` , 80, 110, { align: 'center' });
  doc.moveDown();

  doc.fontSize(10).text( `Lic. José López Portillo, Valle del Cañada, 66059 Cd Gral Escobedo, N.L.` , 80, 130, { align: 'center' });
  doc.moveDown();

  doc.fontSize(10).text( `Farmacia  81 3902 1246 || Oficia 81 3673 7183` , 80, 145, { align: 'center' });
  doc.moveDown();

  doc.rect( 65, 160, 500, 1 ).stroke();
  doc.moveDown();
  doc.moveDown();

  // Nombre:
  doc.fontSize(11).text( `Nombre: ${ nombre_completo }` , 65, 180, { align: 'left' });

  // Sexo
  doc.fontSize(11).text( `Sexo: ${ sexo }` , 300, 180, { align: 'left' });

  // Edad
  doc.fontSize(11).text( `Edad: ${ edad }` , 65, 195, { align: 'left' });

  // Teléfono
  doc.fontSize(11).text( `Teléfono: ${ telefono }` , 300, 195, { align: 'left' });

  // Peso
  doc.fontSize(11).text( `Peso: ${ peso } kg.` , 65, 210, { align: 'left' });

  // FECHA
  doc.fontSize(11).text( `Fecha: ${ hora }` , 300, 210, { align: 'left' });

  doc.rect( 65, 230, 500, 1 ).stroke();

  // Indicaciones
  doc.fontSize(11).text( `Indicaciones:` , 65, 245, { align: 'left' });
  doc.fontSize(11).text( `${ notas }` , 65, 265, { align: 'left' });


  // Indicaciones
  doc.fontSize(11).text( `Diagnóstico:` , 65, 450, { align: 'left' });
  doc.fontSize(11).text( `${ diagnostico }` , 65, 475, { align: 'left' });


  // Pie de paginaaaaa
  doc.rect( 65, 650, 250, 100 ).stroke();
  doc.fontSize(11).text( `Certificado de quien receta - Certificado que: ` , 75, 660, { align: 'left' });
  doc.fontSize(10).text( `- Esta receta es una receta original` , 75, 675, { align: 'left' });
  doc.fontSize(10).text( `- El farmaceutico identificado antedicho es el único` , 75, 685, { align: 'left' });
  doc.fontSize(10).text( `destinatario` , 75, 695, { align: 'left' });
  doc.fontSize(10).text( `- La receta origianl no será reultilizada` , 75, 705, { align: 'left' });

  if( idusuario == 9 ){
    doc.rect( 350, 650, 220, 1 ).stroke();
    doc.fontSize(12).text( `Dr. Ilse María Retana Betancourt` , 350, 660, { align: 'left' });
    doc.fontSize(12).text( `Ced. Prfo: 107433385` , 350, 675, { align: 'left' });
    doc.fontSize(12).text( `UNIVERSIDAD AUTONOMA DE NUEVO LEON` , 350, 690, { align: 'left' });
  }

  if( idusuario == 10 ){
    doc.rect( 350, 650, 220, 1 ).stroke();
    doc.fontSize(12).text( `Dr. Baltazar García García` , 350, 660, { align: 'left' });
    doc.fontSize(12).text( `Ced. Prfo: 10198997` , 350, 675, { align: 'left' });
    doc.fontSize(12).text( `Facultad de Medicina de la Universidad Autónoma de Coahuila` , 350, 690, { align: 'left' });
  }


  doc.end();

}

module.exports = newPDF;
